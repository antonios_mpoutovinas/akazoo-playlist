package com.anmpout.playlist.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.anmpout.playlist.model.PlayList;

import java.util.List;

import static androidx.room.OnConflictStrategy.REPLACE;

/**
 * Created by anmpout on 31/10/2019
 */
@Dao
public interface PlayListDao {
    @Insert(onConflict = REPLACE)
    void save(PlayList playlist);

    @Insert(onConflict = REPLACE)
    void saveAll(List<PlayList> playLists);

    @Query("SELECT * FROM PlayList")
    LiveData<List<PlayList>> loadPlayLists();

    @Query("DELETE  FROM PlayList")
    void deleteAllPlayLists();

    @Query("SELECT count(*) FROM PlayList")
    LiveData<Integer> countPlayLists();

}
