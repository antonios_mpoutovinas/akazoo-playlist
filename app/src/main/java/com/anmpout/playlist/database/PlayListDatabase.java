package com.anmpout.playlist.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.anmpout.playlist.model.PlayList;

/**
 * Created by anmpout on 31/10/2019
 */
@Database(entities = {PlayList.class}, version = 1)

public abstract class PlayListDatabase extends RoomDatabase {

    private static volatile PlayListDatabase INSTANCE;

    public abstract PlayListDao dao();
}
