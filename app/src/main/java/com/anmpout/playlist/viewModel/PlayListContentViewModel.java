package com.anmpout.playlist.viewModel;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.anmpout.playlist.model.Track;
import com.anmpout.playlist.repositories.PlayListRepo;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anmpout on 02/11/2019
 */
public class PlayListContentViewModel extends ViewModel {
    private LiveData<List<Track>> playListsContent;
    private final MutableLiveData<Boolean> loadingData;
    private final MutableLiveData<String> inputPlayListContentArgument;
    private PlayListRepo repo;

    @Inject
    public PlayListContentViewModel(PlayListRepo repo) {
        this.repo = repo;
        loadingData = new MutableLiveData<>();
        loadingData.setValue(true);
        inputPlayListContentArgument = new MutableLiveData<>();

        playListsContent = Transformations.switchMap(inputPlayListContentArgument,
                input -> Transformations.map(repo.getPlayListsContent(input),
                        new Function<List<Track>, List<Track>>() {
                            @Override
                            public List<Track> apply(List<Track> input) {
                                loadingData.setValue(false);
                                return input;
                            }
                        }));

    }


    public void fetchPlayListsData(String playlistId) {
        if (this.playListsContent != null && this.playListsContent.getValue() != null) {
            return;
        }
        inputPlayListContentArgument.setValue(playlistId);


    }

    public LiveData<List<Track>> getPlayListsContent() {
        return playListsContent;
    }

    public MutableLiveData<Boolean> getLoadingData() {
        return loadingData;
    }
}
