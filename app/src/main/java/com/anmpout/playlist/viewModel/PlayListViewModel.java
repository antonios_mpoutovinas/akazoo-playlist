package com.anmpout.playlist.viewModel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.anmpout.playlist.model.PlayList;
import com.anmpout.playlist.network.Resource;
import com.anmpout.playlist.repositories.PlayListRepo;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by anmpout on 31/10/2019
 */
public class PlayListViewModel extends ViewModel {
    private LiveData<Resource<List<PlayList>>> playlistData;
    private final MutableLiveData<Boolean> forceFetch = new MutableLiveData<>();
    private PlayListRepo repo;

    @Inject
    public PlayListViewModel(PlayListRepo repo) {

        this.repo = repo;
        playlistData = Transformations.switchMap(forceFetch,
                input -> Transformations.map(repo.loadPlayLists(input),
                        data -> data));
        forceFetch.postValue(false);
    }


    public void fetchData() {
        forceFetch.postValue(true);

    }

    public LiveData<Resource<List<PlayList>>> getPlaylistData() {
        return this.playlistData;
    }


}
