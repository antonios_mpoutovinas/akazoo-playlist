package com.anmpout.playlist.ui.playlistContent;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.anmpout.playlist.R;
import com.anmpout.playlist.databinding.RowPlayListItemBinding;
import com.anmpout.playlist.model.Track;

import java.util.List;

/**
 * Created by anmpout on 01/11/2019
 */
public class PlayListContentAdapter extends RecyclerView.Adapter<PlayListContentAdapter.PlayListContentViewHolder> {

    private List<Track> trackList;


    @NonNull
    @Override
    public PlayListContentViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RowPlayListItemBinding trackListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.row_play_list_item, viewGroup, false);
        return new PlayListContentViewHolder(trackListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayListContentViewHolder profileViewHolder, int i) {
        Track trackListItem = trackList.get(i);
        profileViewHolder.trackItemBinding.setTrack(trackListItem);

    }


    @Override
    public int getItemCount() {
        if (trackList != null) {
            return trackList.size();
        } else {
            return 0;
        }
    }

    public List<Track> getTrackList() {
        return trackList;
    }

    public void setTrackList(List<Track> trackList) {
        this.trackList = trackList;
        notifyDataSetChanged();
    }

    class PlayListContentViewHolder extends RecyclerView.ViewHolder {

        private RowPlayListItemBinding trackItemBinding;

        public PlayListContentViewHolder(@NonNull final RowPlayListItemBinding trackItemBinding) {
            super(trackItemBinding.getRoot());
            trackItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            this.trackItemBinding = trackItemBinding;
        }


    }
}