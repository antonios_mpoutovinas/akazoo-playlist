package com.anmpout.playlist.ui.playlistContent;

import android.os.Bundle;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.anmpout.playlist.R;

import javax.inject.Inject;

import dagger.android.AndroidInjection;
import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;

/**
 * Created by anmpout on 01/11/2019
 */
public class PlayListContentActivity extends AppCompatActivity implements HasSupportFragmentInjector {
    public static final String PLAYLIST_ID = "playListId";
    public static final String PLAYLIST_NAME = "playListName";
    private String playListId;
    private String playListName;

    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;


    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        return dispatchingAndroidInjector;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parseBundle(savedInstanceState != null ? savedInstanceState : getIntent().getExtras());
        setContentView(R.layout.activity_main);

        this.configureDagger();
        this.showFragment();
        this.initializeActionBar(playListName);
    }

    private void showFragment() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        PlayListContentFragment playListContentFragment = PlayListContentFragment
                .newInstance(new PlayListContentFragment.State(playListId));
        FragmentTransaction fragmentManagerMembersTransaction = fragmentManager.beginTransaction();
        fragmentManagerMembersTransaction.add(R.id.content_frame, playListContentFragment, "");
        fragmentManagerMembersTransaction.commitNow();

    }

    private void configureDagger() {
        AndroidInjection.inject(this);
    }


    private void parseBundle(Bundle bundle) {
        playListId = bundle.getString(PLAYLIST_ID);
        playListName = bundle.getString(PLAYLIST_NAME);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putString(PLAYLIST_ID, playListId);
        outState.putString(PLAYLIST_NAME, playListName);
        super.onSaveInstanceState(outState);
    }

    /**
     * set the title to action bar
     */
    public void initializeActionBar(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayShowTitleEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);
    }
}

