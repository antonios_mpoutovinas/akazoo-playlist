package com.anmpout.playlist.ui.playlistContent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anmpout.playlist.PlayListApplication;
import com.anmpout.playlist.R;
import com.anmpout.playlist.databinding.FragmentPlayListContentBinding;
import com.anmpout.playlist.model.Track;
import com.anmpout.playlist.utils.Utils;
import com.anmpout.playlist.utils.VerticalSpaceItemDecoration;
import com.anmpout.playlist.viewModel.PlayListContentViewModel;

import java.io.Serializable;
import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;


/**
 * Created by anmpout on 01/11/2019
 */
public class PlayListContentFragment extends Fragment {

    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private PlayListContentViewModel viewModel;
    private RecyclerView mRecyclerView;
    private PlayListContentAdapter adapter;
    private static final String STATE = "state";
    private State state;

    public PlayListContentFragment() {
    }

    public static PlayListContentFragment newInstance(State state) {
        PlayListContentFragment fragment = new PlayListContentFragment();
        Bundle args = new Bundle();
        args.putSerializable(STATE, state);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentPlayListContentBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_play_list_content, container, false);
        View rootView = binding.getRoot();
        binding.setLifecycleOwner(this);
        initView(binding);

        return rootView;
    }

    private void initView(FragmentPlayListContentBinding binding) {
        mRecyclerView = binding.recyclerView;
        adapter = new PlayListContentAdapter();
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(Utils.dp(getActivity(), 6)));
        binding.setViewModel(viewModel);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parseBundle(savedInstanceState != null ? savedInstanceState : getArguments());
        configureDagger();
        configureViewModel();
    }

    // -----------------
    // CONFIGURATION
    // -----------------

    private void configureDagger() {
        AndroidSupportInjection.inject(this);
    }

    private void configureViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PlayListContentViewModel.class);
        viewModel.fetchPlayListsData(state.trackId);
        viewModel.getPlayListsContent().observe(this, tracks -> updateUI(tracks));
    }


    private void updateUI(List<Track> trackList) {
//
        if (trackList != null) {
            adapter.setTrackList(trackList);
        } else {
            Toast.makeText(PlayListApplication.context, "Something unexpected happened", Toast.LENGTH_SHORT).show();
        }
    }

    public static class State implements Serializable {
        final String trackId;

        public State(String trackId) {
            this.trackId = trackId;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(STATE, state);
        super.onSaveInstanceState(outState);
    }

    private void parseBundle(Bundle bundle) {
        state = (State) bundle.getSerializable(STATE);
    }
}
