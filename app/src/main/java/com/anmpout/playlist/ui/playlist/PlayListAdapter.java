package com.anmpout.playlist.ui.playlist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.anmpout.playlist.R;
import com.anmpout.playlist.databinding.RowPlayListBinding;
import com.anmpout.playlist.model.PlayList;

import java.util.List;

/**
 * Created by anmpout on 01/11/2019
 */
public class PlayListAdapter extends RecyclerView.Adapter<PlayListAdapter.PlayListViewHolder> {

    private List<PlayList> playLists;
    private ActionListener listener;

    @NonNull
    @Override
    public PlayListViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RowPlayListBinding playListItemBinding =
                DataBindingUtil.inflate(LayoutInflater.from(viewGroup.getContext()),
                        R.layout.row_play_list, viewGroup, false);
        return new PlayListViewHolder(playListItemBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayListViewHolder profileViewHolder, int i) {
        PlayList playListItem = playLists.get(i);
        profileViewHolder.playListItemBinding.setPlayList(playListItem);

    }

    public List<PlayList> getPlayLists() {
        return playLists;
    }

    public void setPlayLists(List<PlayList> playLists) {
        this.playLists = playLists;
        notifyDataSetChanged();
    }

    public ActionListener getListener() {
        return listener;
    }

    public void setListener(ActionListener listener) {
        this.listener = listener;
    }

    @Override
    public int getItemCount() {
        if (playLists != null) {
            return playLists.size();
        } else {
            return 0;
        }
    }

    class PlayListViewHolder extends RecyclerView.ViewHolder {

        private RowPlayListBinding playListItemBinding;

        public PlayListViewHolder(@NonNull final RowPlayListBinding playListItemBinding) {
            super(playListItemBinding.getRoot());
            playListItemBinding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (listener != null) {
                        listener.onOpenPlayList(playListItemBinding.getPlayList());

                    }
                }
            });
            this.playListItemBinding = playListItemBinding;
        }


    }

    /**
     * The interface Action listener.
     */
    interface ActionListener {
        /**
         * On open playlist.
         *
         * @param playList the selected playlist
         */
        void onOpenPlayList(PlayList playList);


    }

}
