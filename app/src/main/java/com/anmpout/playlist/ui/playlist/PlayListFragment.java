package com.anmpout.playlist.ui.playlist;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.anmpout.playlist.R;
import com.anmpout.playlist.databinding.FragmentPlayListBinding;
import com.anmpout.playlist.model.PlayList;
import com.anmpout.playlist.network.Resource;
import com.anmpout.playlist.ui.playlistContent.PlayListContentActivity;
import com.anmpout.playlist.utils.Utils;
import com.anmpout.playlist.utils.VerticalSpaceItemDecoration;
import com.anmpout.playlist.viewModel.PlayListViewModel;

import java.util.List;

import javax.inject.Inject;

import dagger.android.support.AndroidSupportInjection;

import static com.anmpout.playlist.network.Resource.ERROR;
import static com.anmpout.playlist.network.Resource.SUCCESS;
import static com.anmpout.playlist.ui.playlistContent.PlayListContentActivity.PLAYLIST_ID;
import static com.anmpout.playlist.ui.playlistContent.PlayListContentActivity.PLAYLIST_NAME;

/**
 * Created by anmpout on 31/10/2019
 */
public class PlayListFragment extends Fragment implements PlayListAdapter.ActionListener {


    @Inject
    ViewModelProvider.Factory viewModelFactory;
    private PlayListViewModel viewModel;
    private RecyclerView mRecyclerView;
    private PlayListAdapter adapter;

    public PlayListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentPlayListBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_play_list, container, false);
        View rootView = binding.getRoot();
        binding.setLifecycleOwner(this);
        initView(binding);

        return rootView;
    }

    private void initView(FragmentPlayListBinding binding) {
        mRecyclerView = binding.recyclerView;
        adapter = new PlayListAdapter();
        adapter.setListener(this);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        mRecyclerView.addItemDecoration(new VerticalSpaceItemDecoration(Utils.dp(getActivity(), 6)));
        binding.refreshButton.setOnClickListener(view -> viewModel.fetchData());
        binding.setViewModel(viewModel);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        configureDagger();
        configureViewModel();
    }

    private void configureDagger() {
        AndroidSupportInjection.inject(this);
    }

    private void configureViewModel() {
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(PlayListViewModel.class);
        viewModel.getPlaylistData().observe(this, this::updateUI);
    }


    private void updateUI(Resource<List<PlayList>> playLists) {
        switch (playLists.status) {
            case SUCCESS:
                adapter.setPlayLists(playLists.data);
                break;
            case ERROR:
                Toast.makeText(getContext(), "Something unexpected happened!Please try again later!", Toast.LENGTH_LONG).show();
                break;
            default:
                break;

        }
    }

    @Override
    public void onOpenPlayList(PlayList playlist) {
        Intent intent = new Intent(getActivity(), PlayListContentActivity.class);
        Bundle extras = new Bundle();
        extras.putString(PLAYLIST_ID, playlist.getPlaylistId());
        extras.putString(PLAYLIST_NAME, playlist.getName());
        intent.putExtras(extras);
        startActivity(intent);
    }
}


