package com.anmpout.playlist;

import android.app.Activity;
import android.app.Application;
import android.content.Context;

import com.anmpout.playlist.di.DaggerAppComponent;

import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.HasActivityInjector;

/**
 * Created by anmpout on 31/10/2019
 */
public class PlayListApplication extends Application implements HasActivityInjector {

    @Inject
    DispatchingAndroidInjector<Activity> dispatchingAndroidInjector;

    public static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        this.initDagger();
        context = getApplicationContext();
    }

    @Override
    public DispatchingAndroidInjector<Activity> activityInjector() {
        return dispatchingAndroidInjector;
    }
    
    private void initDagger() {
        DaggerAppComponent.builder().application(this).build().inject(this);
    }
}
