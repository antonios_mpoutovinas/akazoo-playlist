package com.anmpout.playlist.network;

import com.anmpout.playlist.model.Response_Playlist;
import com.anmpout.playlist.model.Response_TrackList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by anmpout on 31/10/2019
 */
public interface PlayListWebservice {
    @GET("playlists")
    Call<Response_Playlist> getPlayList();

    @GET("playlist")
    Call<Response_TrackList> getPlayListTracks(@Query(value = "playlistid") String playlistId);
}

