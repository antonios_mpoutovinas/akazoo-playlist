package com.anmpout.playlist.network;

/**
 * Created by anmpout on 03/11/2019
 */


import androidx.annotation.MainThread;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;

import com.anmpout.playlist.AppExecutors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public abstract class NetworkBoundResource<ResultType, RequestType> {
    private MediatorLiveData<Resource<ResultType>> result = new MediatorLiveData<>();
    private AppExecutors appExecutor;

    @MainThread
    public NetworkBoundResource(AppExecutors appExecutor) {
        this.appExecutor = appExecutor;
        result.setValue(Resource.loading(null));
        if (forceFetch()) {
            fetchFromNetwork(null);
            return;
        }
        LiveData<ResultType> dbSource = loadFromDb();

        result.addSource(dbSource, data -> {
            result.removeSource(dbSource);
            if (shouldFetch(data)) {
                fetchFromNetwork(dbSource);
            } else {
                result.addSource(dbSource, newData -> result.setValue(Resource.success(newData)));
            }
        });
    }

    private void fetchFromNetwork(final LiveData<ResultType> dbSource) {
        if (forceFetch()) {
            result.setValue(Resource.loading(null));
        } else {
            result.addSource(dbSource, newData -> result.setValue(Resource.loading(newData)));

        }
        createCall().enqueue(new Callback<RequestType>() {
            @Override
            public void onResponse(Call<RequestType> call, Response<RequestType> response) {
                result.removeSource(dbSource);
                appExecutor.diskIO().execute(() -> {
                    saveCallResult(response.body());
                    appExecutor.mainThread().execute(() ->
                            result.addSource(loadFromDb(), newData ->
                                    result.setValue(Resource.success(newData))));
                });
            }

            @Override
            public void onFailure(Call<RequestType> call, Throwable t) {
                onFetchFailed();
                result.removeSource(dbSource);
                result.addSource(dbSource, newData -> result.setValue(Resource.error(t.getMessage(), newData)));
            }
        });
    }


    @MainThread
    protected boolean forceFetch() {
        return false;
    }

    @WorkerThread
    protected abstract void saveCallResult(@NonNull RequestType item);

    @MainThread
    protected boolean shouldFetch(@Nullable ResultType data) {
        return true;
    }

    @NonNull
    @MainThread
    protected abstract LiveData<ResultType> loadFromDb();

    @NonNull
    @MainThread
    protected abstract Call<RequestType> createCall();

    @MainThread
    protected void onFetchFailed() {
    }

    public final MediatorLiveData<Resource<ResultType>> getAsLiveData() {
        return result;
    }
}




