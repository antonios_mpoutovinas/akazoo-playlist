package com.anmpout.playlist.repositories;

import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.anmpout.playlist.AppExecutors;
import com.anmpout.playlist.PlayListApplication;
import com.anmpout.playlist.database.PlayListDao;
import com.anmpout.playlist.model.PlayList;
import com.anmpout.playlist.model.Response_Playlist;
import com.anmpout.playlist.model.Response_TrackList;
import com.anmpout.playlist.model.Track;
import com.anmpout.playlist.network.NetworkBoundResource;
import com.anmpout.playlist.network.PlayListWebservice;
import com.anmpout.playlist.network.Resource;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by anmpout on 31/10/2019
 */
@Singleton
public class PlayListRepo {
    private final PlayListWebservice webservice;
    private final PlayListDao playlistDao;
    private final AppExecutors executor;

    @Inject
    public PlayListRepo(PlayListWebservice webservice, PlayListDao playlistDao, AppExecutors executor) {
        this.webservice = webservice;
        this.playlistDao = playlistDao;
        this.executor = executor;
    }


    public MutableLiveData<List<Track>> getPlayListsContent(String playlistId) {
        MutableLiveData<List<Track>> tracks = new MutableLiveData<>();
        webservice.getPlayListTracks(playlistId).enqueue(new Callback<Response_TrackList>() {
            @Override
            public void onResponse(Call<Response_TrackList> call,
                                   Response<Response_TrackList> response) {
                if (response.isSuccessful()) {
                    Response_TrackList responsePlaylist = response.body();
                    tracks.postValue(responsePlaylist.getResultTrackList().getResultTrackList());

                }
            }

            @Override
            public void onFailure(Call<Response_TrackList> call, Throwable t) {
                Toast.makeText(PlayListApplication.context, t.getMessage(), Toast.LENGTH_LONG).show();
                tracks.postValue(null);
            }
        });

        return tracks;
    }


    public MediatorLiveData<Resource<List<PlayList>>> loadPlayLists(Boolean forceFetch) {
        return new NetworkBoundResource<List<PlayList>, Response_Playlist>(executor) {

            @Override
            protected boolean forceFetch() {
                return forceFetch;
            }

            @Override
            protected void saveCallResult(@NonNull Response_Playlist item) {
                playlistDao.saveAll(item.getResultPlaylist());
            }

            @NonNull
            @Override
            protected LiveData<List<PlayList>> loadFromDb() {
                return playlistDao.loadPlayLists();
            }

            @NonNull
            @Override
            protected Call<Response_Playlist> createCall() {
                return webservice.getPlayList();
            }

            @NonNull
            @Override
            protected boolean shouldFetch(@Nullable List<PlayList> data) {
                return (forceFetch || (data != null && data.isEmpty()));
            }
        }.getAsLiveData();
    }


}