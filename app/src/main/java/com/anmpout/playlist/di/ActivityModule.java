package com.anmpout.playlist.di;

import com.anmpout.playlist.ui.playlist.PlayListActivity;
import com.anmpout.playlist.ui.playlistContent.PlayListContentActivity;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by anmpout on 31/10/2019
 */
@Module
public abstract class ActivityModule {
    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract PlayListActivity contributeMainActivity();

    @ContributesAndroidInjector(modules = FragmentModule.class)
    abstract PlayListContentActivity contributePlayListContentActivity();
}
