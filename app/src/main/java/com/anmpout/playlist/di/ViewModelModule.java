package com.anmpout.playlist.di;

import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.anmpout.playlist.viewModel.PlayListContentViewModel;
import com.anmpout.playlist.viewModel.PlayListViewModel;
import com.anmpout.playlist.viewModel.ViewModelFactory;

import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

/**
 * Created by anmpout on 31/10/2019
 */
@Module
public abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(PlayListViewModel.class)
    abstract ViewModel bindPlayListViewModel(PlayListViewModel repoViewModel);

    @Binds
    @IntoMap
    @ViewModelKey(PlayListContentViewModel.class)
    abstract ViewModel bindPlayListContentViewModel(PlayListContentViewModel repoViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(ViewModelFactory factory);
}
