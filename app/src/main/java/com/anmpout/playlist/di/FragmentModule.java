package com.anmpout.playlist.di;

import com.anmpout.playlist.ui.playlist.PlayListFragment;
import com.anmpout.playlist.ui.playlistContent.PlayListContentFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

/**
 * Created by anmpout on 31/10/2019
 */
@Module
public abstract class FragmentModule {
    @ContributesAndroidInjector
    abstract PlayListFragment contributePlayListFragment();

    @ContributesAndroidInjector
    abstract PlayListContentFragment contributePlayListContentFragment();
}
