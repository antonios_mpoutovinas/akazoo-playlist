package com.anmpout.playlist.di;

import android.app.Application;

import androidx.room.Room;

import com.anmpout.playlist.AppExecutors;
import com.anmpout.playlist.database.PlayListDao;
import com.anmpout.playlist.database.PlayListDatabase;
import com.anmpout.playlist.network.PlayListWebservice;
import com.anmpout.playlist.repositories.PlayListRepo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by anmpout on 31/10/2019
 */
@Module(includes = ViewModelModule.class)
public class AppModule {


    @Provides
    @Singleton
    PlayListDatabase provideDatabase(Application application) {
        return Room.databaseBuilder(application,
                PlayListDatabase.class, "playListDatabase.db")
                .build();
    }

    @Provides
    @Singleton
    PlayListDao providePlayListDao(PlayListDatabase database) {
        return database.dao();
    }


    @Provides
    AppExecutors provideExecutor() {
        return new AppExecutors();
    }

    @Provides
    @Singleton
    PlayListRepo provideUserRepository(PlayListWebservice webservice, PlayListDao dao, AppExecutors executor) {
        return new PlayListRepo(webservice, dao, executor);
    }


    private static String BASE_URL = "http://akazoo.com/services/Test/TestMobileService.svc/";

    @Provides
    Gson provideGson() {
        return new GsonBuilder().create();
    }

    @Provides
    Retrofit provideRetrofit(Gson gson) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(BASE_URL)
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    PlayListWebservice provideApiWebservice(Retrofit restAdapter) {
        return restAdapter.create(PlayListWebservice.class);
    }
}