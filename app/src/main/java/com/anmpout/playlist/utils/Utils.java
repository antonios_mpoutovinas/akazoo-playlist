package com.anmpout.playlist.utils;

import android.content.Context;

/**
 * Created by anmpout on 02/11/2019
 */
public class Utils {
    /**
     * Coverts pixel to dp.
     *
     * @param activity the activity
     * @param pixels   the pixels
     * @return the int
     */
    public static int dp(Context activity, int pixels) {
        float density = activity.getResources().getDisplayMetrics().density;
        return (int) (pixels * density);
    }

}
