package com.anmpout.playlist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anmpout on 31/10/2019
 */
public class Track {

    @SerializedName("ItemId")
    @Expose
    private Integer ItemId;

    @SerializedName("Position")
    @Expose
    private Integer Position;

    @SerializedName("StreamingModuleId")
    @Expose
    private String StreamingModuleId;

    @SerializedName("AlbumId")
    @Expose
    private Integer AlbumId;

    @SerializedName("AlbumName")
    @Expose
    private String AlbumName;

    @SerializedName("ImageUrl")
    @Expose
    private String ImageUrl;

    @SerializedName("LargeImageUrl")
    @Expose
    private String LargeImageUrl;

    @SerializedName("DateUserAdded")
    @Expose
    private String DateUserAdded;

    @SerializedName("DateUserAddedIso")
    @Expose
    private String DateUserAddedIso;

    @SerializedName("TrackId")
    @Expose
    private Integer TrackId;

    @SerializedName("ModuleId")
    @Expose
    private String ModuleId;

    @SerializedName("TrackName")
    @Expose
    private String TrackName;

    @SerializedName("TrackNumber")
    @Expose
    private Integer TrackNumber;

    @SerializedName("TrackDuration")
    @Expose
    private Integer TrackDuration;

    @SerializedName("IsUserFan")
    @Expose
    private Boolean IsUserFan;

    @SerializedName("UserMark")
    @Expose
    private Integer UserMark;

    @SerializedName("IsOwner")
    @Expose
    private Boolean IsOwner;

    @SerializedName("IsStreamable")
    @Expose
    private Boolean IsStreamable;

    @SerializedName("IsPreview")
    @Expose
    private Boolean IsPreview;

    @SerializedName("IsExplicit")
    @Expose
    private Boolean IsExplicit;

    @SerializedName("ArtistId")
    @Expose
    private Integer ArtistId;

    @SerializedName("ArtistName")
    @Expose
    private String ArtistName;

    @SerializedName("Genres")
    @Expose
    private String Genres;

    @SerializedName("ObjectType")
    @Expose
    private String ObjectType;

    public Track() {
    }

    public Track(Integer itemId, Integer position, String streamingModuleId, Integer albumId, String albumName, String imageUrl, String largeImageUrl, String dateUserAdded, String dateUserAddedIso, Integer trackId, String moduleId, String trackName, Integer trackNumber, Integer trackDuration, Boolean isUserFan, Integer userMark, Boolean isOwner, Boolean isStreamable, Boolean isPreview, Boolean isExplicit, Integer artistId, String artistName, String genres, String objectType) {
        ItemId = itemId;
        Position = position;
        StreamingModuleId = streamingModuleId;
        AlbumId = albumId;
        AlbumName = albumName;
        ImageUrl = imageUrl;
        LargeImageUrl = largeImageUrl;
        DateUserAdded = dateUserAdded;
        DateUserAddedIso = dateUserAddedIso;
        TrackId = trackId;
        ModuleId = moduleId;
        TrackName = trackName;
        TrackNumber = trackNumber;
        TrackDuration = trackDuration;
        IsUserFan = isUserFan;
        UserMark = userMark;
        IsOwner = isOwner;
        IsStreamable = isStreamable;
        IsPreview = isPreview;
        IsExplicit = isExplicit;
        ArtistId = artistId;
        ArtistName = artistName;
        Genres = genres;
        ObjectType = objectType;
    }

    public Integer getItemId() {
        return ItemId;
    }

    public void setItemId(Integer itemId) {
        ItemId = itemId;
    }

    public Integer getPosition() {
        return Position;
    }

    public void setPosition(Integer position) {
        Position = position;
    }

    public String getStreamingModuleId() {
        return StreamingModuleId;
    }

    public void setStreamingModuleId(String streamingModuleId) {
        StreamingModuleId = streamingModuleId;
    }

    public Integer getAlbumId() {
        return AlbumId;
    }

    public void setAlbumId(Integer albumId) {
        AlbumId = albumId;
    }

    public String getAlbumName() {
        return AlbumName;
    }

    public void setAlbumName(String albumName) {
        AlbumName = albumName;
    }

    public String getImageUrl() {
        return ImageUrl;
    }

    public void setImageUrl(String imageUrl) {
        ImageUrl = imageUrl;
    }

    public String getLargeImageUrl() {
        return LargeImageUrl;
    }

    public void setLargeImageUrl(String largeImageUrl) {
        LargeImageUrl = largeImageUrl;
    }

    public String getDateUserAdded() {
        return DateUserAdded;
    }

    public void setDateUserAdded(String dateUserAdded) {
        DateUserAdded = dateUserAdded;
    }

    public String getDateUserAddedIso() {
        return DateUserAddedIso;
    }

    public void setDateUserAddedIso(String dateUserAddedIso) {
        DateUserAddedIso = dateUserAddedIso;
    }

    public Integer getTrackId() {
        return TrackId;
    }

    public void setTrackId(Integer trackId) {
        TrackId = trackId;
    }

    public String getModuleId() {
        return ModuleId;
    }

    public void setModuleId(String moduleId) {
        ModuleId = moduleId;
    }

    public String getTrackName() {
        return TrackName;
    }

    public void setTrackName(String trackName) {
        TrackName = trackName;
    }

    public Integer getTrackNumber() {
        return TrackNumber;
    }

    public void setTrackNumber(Integer trackNumber) {
        TrackNumber = trackNumber;
    }

    public Integer getTrackDuration() {
        return TrackDuration;
    }

    public void setTrackDuration(Integer trackDuration) {
        TrackDuration = trackDuration;
    }

    public Boolean getUserFan() {
        return IsUserFan;
    }

    public void setUserFan(Boolean userFan) {
        IsUserFan = userFan;
    }

    public Integer getUserMark() {
        return UserMark;
    }

    public void setUserMark(Integer userMark) {
        UserMark = userMark;
    }

    public Boolean getOwner() {
        return IsOwner;
    }

    public void setOwner(Boolean owner) {
        IsOwner = owner;
    }

    public Boolean getStreamable() {
        return IsStreamable;
    }

    public void setStreamable(Boolean streamable) {
        IsStreamable = streamable;
    }

    public Boolean getPreview() {
        return IsPreview;
    }

    public void setPreview(Boolean preview) {
        IsPreview = preview;
    }

    public Boolean getExplicit() {
        return IsExplicit;
    }

    public void setExplicit(Boolean explicit) {
        IsExplicit = explicit;
    }

    public Integer getArtistId() {
        return ArtistId;
    }

    public void setArtistId(Integer artistId) {
        ArtistId = artistId;
    }

    public String getArtistName() {
        return ArtistName;
    }

    public void setArtistName(String artistName) {
        ArtistName = artistName;
    }

    public String getGenres() {
        return Genres;
    }

    public void setGenres(String genres) {
        Genres = genres;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }
}
