package com.anmpout.playlist.model;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anmpout on 31/10/2019
 */
@Entity
public class PlayList {

    @PrimaryKey
    @NonNull
    @SerializedName("PlaylistId")
    @Expose
    private String PlaylistId;

    @SerializedName("Name")
    @Expose
    private String Name;

    @SerializedName("Duration")
    @Expose
    private Integer Duration;

    @SerializedName("ItemCount")
    @Expose
    private Integer ItemCount;

    @SerializedName("IsOwner")
    @Expose
    private Boolean isOwner;

    @SerializedName("DateUpdated")
    @Expose
    private String DateUpdated;

    @SerializedName("DateUpdatedIso")
    @Expose
    private String DateUpdatedIso;

    @SerializedName("OwnerId")
    @Expose
    private String OwnerId;

    @SerializedName("OwnerNickName")
    @Expose
    private String OwnerNickName;

    @SerializedName("OwnerPhotoUrl")
    @Expose
    private String OwnerPhotoUrl;

    @SerializedName("ViewerIsFan")
    @Expose
    private Boolean ViewerIsFan;

    @SerializedName("FanCount")
    @Expose
    private Integer FanCount;

    @SerializedName("PhotoUrl")
    @Expose
    private String PhotoUrl;

    @SerializedName("LargePhotoUrl")
    @Expose
    private String LargePhotoUrl;

    @SerializedName("ObjectType")
    @Expose
    private String ObjectType;

    public PlayList() {
    }

    public PlayList(@NonNull String playlistId, String name, Integer duration, Integer itemCount, Boolean isOwner, String dateUpdated, String dateUpdatedIso, String ownerId, String ownerNickName, String ownerPhotoUrl, Boolean viewerIsFan, Integer fanCount, String photoUrl, String largePhotoUrl, String objectType) {
        PlaylistId = playlistId;
        Name = name;
        Duration = duration;
        ItemCount = itemCount;
        this.isOwner = isOwner;
        DateUpdated = dateUpdated;
        DateUpdatedIso = dateUpdatedIso;
        OwnerId = ownerId;
        OwnerNickName = ownerNickName;
        OwnerPhotoUrl = ownerPhotoUrl;
        ViewerIsFan = viewerIsFan;
        FanCount = fanCount;
        PhotoUrl = photoUrl;
        LargePhotoUrl = largePhotoUrl;
        ObjectType = objectType;
    }

    @NonNull
    public String getPlaylistId() {
        return PlaylistId;
    }

    public void setPlaylistId(@NonNull String playlistId) {
        PlaylistId = playlistId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public Integer getDuration() {
        return Duration;
    }

    public void setDuration(Integer duration) {
        Duration = duration;
    }

    public Integer getItemCount() {
        return ItemCount;
    }

    public void setItemCount(Integer itemCount) {
        ItemCount = itemCount;
    }

    public Boolean getOwner() {
        return isOwner;
    }

    public void setOwner(Boolean owner) {
        isOwner = owner;
    }

    public String getDateUpdated() {
        return DateUpdated;
    }

    public void setDateUpdated(String dateUpdated) {
        DateUpdated = dateUpdated;
    }

    public String getDateUpdatedIso() {
        return DateUpdatedIso;
    }

    public void setDateUpdatedIso(String dateUpdatedIso) {
        DateUpdatedIso = dateUpdatedIso;
    }

    public String getOwnerId() {
        return OwnerId;
    }

    public void setOwnerId(String ownerId) {
        OwnerId = ownerId;
    }

    public String getOwnerNickName() {
        return OwnerNickName;
    }

    public void setOwnerNickName(String ownerNickName) {
        OwnerNickName = ownerNickName;
    }

    public String getOwnerPhotoUrl() {
        return OwnerPhotoUrl;
    }

    public void setOwnerPhotoUrl(String ownerPhotoUrl) {
        OwnerPhotoUrl = ownerPhotoUrl;
    }

    public Boolean getViewerIsFan() {
        return ViewerIsFan;
    }

    public void setViewerIsFan(Boolean viewerIsFan) {
        ViewerIsFan = viewerIsFan;
    }

    public Integer getFanCount() {
        return FanCount;
    }

    public void setFanCount(Integer fanCount) {
        FanCount = fanCount;
    }

    public String getPhotoUrl() {
        return PhotoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        PhotoUrl = photoUrl;
    }

    public String getLargePhotoUrl() {
        return LargePhotoUrl;
    }

    public void setLargePhotoUrl(String largePhotoUrl) {
        LargePhotoUrl = largePhotoUrl;
    }

    public String getObjectType() {
        return ObjectType;
    }

    public void setObjectType(String objectType) {
        ObjectType = objectType;
    }


}
