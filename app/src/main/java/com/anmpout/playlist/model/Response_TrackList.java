package com.anmpout.playlist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by anmpout on 02/11/2019
 */
public class Response_TrackList {

    @SerializedName("Result")
    @Expose
    private Result_TrackList resultTrackList;

    public Response_TrackList() {

    }

    public Response_TrackList(Result_TrackList resultTrackList) {
        this.resultTrackList = resultTrackList;
    }

    public Result_TrackList getResultTrackList() {
        return resultTrackList;
    }

    public void setResultTrackList(Result_TrackList resultTrackList) {
        this.resultTrackList = resultTrackList;
    }
}
