package com.anmpout.playlist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anmpout on 02/11/2019
 */
public class Result_TrackList {

    @SerializedName("Items")
    @Expose
    private List<Track> resultTrackList;

    public Result_TrackList() {
        resultTrackList = new ArrayList<>();
    }

    public List<Track> getResultTrackList() {
        return resultTrackList;
    }

    public void setResultTrackList(List<Track> resultTrackList) {
        this.resultTrackList = resultTrackList;
    }
}
