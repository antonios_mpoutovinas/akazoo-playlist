package com.anmpout.playlist.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anmpout on 31/10/2019
 */
public class Response_Playlist {

    @SerializedName("Result")
    @Expose
    private List<PlayList> resultPlaylist;

    public Response_Playlist() {
        resultPlaylist = new ArrayList<>();
    }

    public Response_Playlist(List<PlayList> resultPlaylist) {
        this.resultPlaylist = resultPlaylist;
    }

    public List<PlayList> getResultPlaylist() {
        return resultPlaylist;
    }

    public void setResultPlaylist(List<PlayList> resultPlaylist) {
        this.resultPlaylist = resultPlaylist;
    }
}
