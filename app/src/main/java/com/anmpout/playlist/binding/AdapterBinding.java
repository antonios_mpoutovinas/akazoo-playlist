package com.anmpout.playlist.binding;

import android.graphics.Color;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.databinding.BindingAdapter;

import com.squareup.picasso.Picasso;

/**
 * Created by anmpout on 17/09/2019
 */
public class AdapterBinding {

    @BindingAdapter("visibleGone")
    public static void visibleGone(View view, Boolean isVisible) {
        if (isVisible == null) {
            return;
        }
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.GONE);

        }
    }

    @BindingAdapter("visibleInvisible")
    public static void visibleInvisible(View view, Boolean isVisible) {
        if (isVisible == null) {
            return;
        }
        if (isVisible) {
            view.setVisibility(View.VISIBLE);
        } else {
            view.setVisibility(View.INVISIBLE);

        }
    }

    @BindingAdapter("setTextColor")
    public static void setTextColor(TextView textView, String hex) {
        if (hex == null) {
            return;
        }
        textView.setTextColor(Color.parseColor(hex));
    }

    @BindingAdapter("loadImgUrl")
    public static void loadImageUrl(ImageView view, String loadImgUrl) {
        if (loadImgUrl == null) {
            return;
        }
        Picasso.get()
                .load(loadImgUrl)
                .into(view);
    }


}
