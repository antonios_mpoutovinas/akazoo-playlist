package com.anmpout.playlist.repositories;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.anmpout.playlist.AppExecutors;
import com.anmpout.playlist.database.PlayListDao;
import com.anmpout.playlist.database.PlayListDatabase;
import com.anmpout.playlist.model.PlayList;
import com.anmpout.playlist.network.PlayListWebservice;
import com.anmpout.playlist.network.Resource;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by anmpout on 03/11/2019
 */
@RunWith(JUnit4.class)
public class PlayListRepoTest {
    private PlayListDao playListDao;
    private PlayListWebservice webservice;
    private PlayListRepo repo;
    private AppExecutors appExecutors;


    @Before
    public void setUp() throws Exception {
        playListDao = mock(PlayListDao.class);
        webservice = mock(PlayListWebservice.class);
        appExecutors = mock(AppExecutors.class);
        PlayListDatabase db = mock(PlayListDatabase.class);
        when(db.dao()).thenReturn(playListDao);
        repo = new PlayListRepo(webservice,playListDao,appExecutors);
    }

    @Test
    public void getPlayListsOffline() {
    }

    @Test
    public void getPlayListsContent() {

    }
}